# -g flags para debug
# -Wall ativar warnings

CXX = g++
CXXFLAGS= -g -Wall


all: matriz.o
	# Compile and link the source and object files into a final executable binary
	$(CXX) $(CXXFLAGS) main.cpp matriz.o

%.o: %/%.cpp %/%.h
	$(CXX) $(CXXFLAGS) -g -c %/%.cpp

clean:
	# remove all binary object files
	rm -f *.o 
	# remove binary executable
	rm -f a.out
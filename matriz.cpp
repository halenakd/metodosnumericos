#include "matriz.h"

// inicializa so com as quantidades, pra depois colocar os numeros com o setElemento
Matriz::Matriz(int linhas, int colunas):
    linhas(linhas), colunas(colunas) 
{
    matriz.resize(linhas, vector<float>(colunas, 0));
}

// inicializa com as quantidades e ja colocando um vector de vectors tambem (caso de vetores solucao, por ex.)
Matriz::Matriz(int linhas, int colunas, vector<vector<float>> solucao):
    Matriz(linhas, colunas)
{
    for(int l = 0; l < linhas; l++)
    {
        for(int c = 0; c < colunas; c++)
        {
            matriz[l][c] = solucao[c][l];
        }
    }
}

// destrutor
Matriz::~Matriz()
{

}

// seta os numeros
void Matriz::setElemento(int linha, int coluna, int valor)
{
    matriz[linha][coluna] = valor;
}

// retorna um numero
int Matriz::getElemento(int linha, int coluna) const 
{
    return matriz[linha][coluna];
}

// imprime a matriz
void Matriz::imprimir() const 
{
    for (int l = 0; l < linhas; ++l) 
    {
        for (int c = 0; c < colunas; ++c) 
        {
            cout << matriz[l][c] << " ";
        }
        cout << endl;
    }
}

// soma matrizes
Matriz Matriz::operator+(const Matriz& n) const
{
    if(linhas == n.linhas && colunas == n.colunas)
    {
        Matriz resultado(linhas, colunas);

        for(int l = 0; l < linhas; l++)
        {
            for(int c = 0; c < n.colunas; c++)
            {
                resultado.matriz[l][c] = matriz[l][c] + n.matriz[l][c];
            }
        }

        return resultado;
    }
    throw runtime_error("numero de linhas e colunas deve ser o mesmo");
}

// subtrai matrizes
Matriz Matriz::operator-(const Matriz& n) const
{
    if(linhas == n.linhas && colunas == n.colunas)
    {
        Matriz resultado(linhas, colunas);

        for(int l = 0; l < linhas; l++)
        {
            for(int c = 0; c < n.colunas; c++)
            {
                resultado.matriz[l][c] = matriz[l][c] - n.matriz[l][c];
            }
        }

        return resultado;
    }
    throw runtime_error("numero de linhas e colunas deve ser o mesmo");
}

/*
// multiplica matrizes
Matriz Matriz::operator*(const Matriz& n) const
{
    // nao eh possivel fazer a multiplicacao se o numero de colunas de m for diferente do numero de linhas de n
    if (colunas != n.linhas)
    {
        throw runtime_error("numero de colunas da primeira matriz eh diferente do numero de linhas da segunda");
    }

    // a matriz nova vai ter o numero de linhas de m e o numero de colunas de n
    Matriz resultado(linhas, n.colunas);

    for(int l = 0; l < linhas; l++)
    {
        for(int c = 0; c < n.colunas; c++) 
        {
            for(int cNova = 0; cNova < colunas; cNova++) 
            {
                resultado.matriz[l][c] += matriz[l][c] * n.matriz[cNova][c];
            }
        }
    }

    return resultado;
}
*/
Matriz Matriz::operator*(const Matriz& n) const
{
    // Verificar se a multiplicação é possível
    if (colunas != n.linhas)
    {
        throw runtime_error("numero de colunas da primeira matriz eh diferente do numero de linhas da segunda");
    }

    Matriz resultado(linhas, n.colunas);

    for (int l = 0; l < linhas; l++)
    {
        for (int c = 0; c < n.colunas; c++)
        {
            float soma = 0;
            for (int cNova = 0; cNova < colunas; cNova++)
            {
                soma += matriz[l][cNova] * n.matriz[cNova][c];
            }
            resultado.matriz[l][c] = soma;
        }
    }

    return resultado;
}


// soma matriz com escalar
Matriz Matriz::operator+(int i) const
{
    Matriz resultado(linhas, colunas);

    for(int l = 0; l < linhas; l++)
    {
        for(int c = 0; c < colunas; c++)
        {
            resultado.matriz[l][c] = matriz[l][c] + i;
        }
    }

    return resultado;
}

// subtrai escalar de matriz
Matriz Matriz::operator-(int i) const
{
    Matriz resultado(linhas, colunas);

    for(int l = 0; l < linhas; l++)
    {
        for(int c = 0; c < colunas; c++)
        {
            resultado.matriz[l][c] = matriz[l][c] - i;
        }
    }

    return resultado;
}

// multiplica matriz por escalar
Matriz Matriz::operator*(int i) const
{
    Matriz resultado(linhas, colunas);

    for(int l = 0; l < linhas; l++)
    {
        for(int c = 0; c < colunas; c++)
        {
            resultado.matriz[l][c] = matriz[l][c] * i;
        }
    }

    return resultado;
}

// divide matriz por escalar
Matriz Matriz::operator/(int i) const
{
    Matriz resultado(linhas, colunas);

    for(int l = 0; l < linhas; l++)
    {
        for(int c = 0; c < colunas; c++)
        {
            resultado.matriz[l][c] = matriz[l][c] / i;
        }
    }

    return resultado;
}

Matriz Matriz::T() const
{
    Matriz resultado(linhas, colunas);
    for(int l = 0; l < linhas; l++)
    {
        for(int c = 0; c < colunas; c++)
        {
            resultado.matriz[l][c] = matriz[c][l];
        }
    }
    return resultado;
}


// triangular superior = zera o triangulo de baixo
Matriz Matriz::gauss(Matriz& vetorSolucao) const
{
    Matriz escalonada(linhas, colunas);

    escalonada.matriz = matriz;

    int posPivo; // posicao do pivo/coluna dos "pivos" de cada linha abaixo

    int lInicio = 1; // eh 1 pq a primeira linha (0) nao sofre alteracao

    float multiplicador; // eh o primeiroDaLinha/pivo
    // pivo ta sempre na diagonal, eh o que vai multiplicar a linha que esta sofrendo a alteracao (ver caderno)
    // primeiroDaLinha eh o primeiro numero que nao eh zero na linha que esta sofrendo a alteracao, vai multiplicar a linha do pivo (ver caderno)

    // zerando parte inferior
    for(posPivo = 0; posPivo < linhas; posPivo++)
    {
        for(int l = lInicio; l < linhas; l++)
        {
            multiplicador = escalonada.matriz[l][posPivo] / escalonada.matriz[posPivo][posPivo];
            for(int c = posPivo; c < colunas; c++)
            {
                escalonada.matriz[l][c] = escalonada.matriz[l][c] - multiplicador * escalonada.matriz[posPivo][c];
            }
            // alterando os valores do vetorSolucao tambem
            for(int i = 0; i < vetorSolucao.colunas; i++)
            {
                vetorSolucao.matriz[l][i] = vetorSolucao.matriz[l][i] - multiplicador * vetorSolucao.matriz[posPivo][i]; // faz as operacoes no vetorSolucao tambem, alem de na matriz
            }
        }
        //escalonada.imprimir();
        ++lInicio;
    }
    return escalonada;
}

Matriz Matriz::gauss() const
{
    Matriz escalonada(linhas, colunas);

    escalonada.matriz = matriz;

    int posPivo; // posicao do pivo/coluna dos "pivos" de cada linha abaixo

    int lInicio = 1; // eh 1 pq a primeira linha (0) nao sofre alteracao

    float multiplicador; // eh o primeiroDaLinha/pivo
    // pivo ta sempre na diagonal, eh o que vai multiplicar a linha que esta sofrendo a alteracao (ver caderno)
    // primeiroDaLinha eh o primeiro numero que nao eh zero na linha que esta sofrendo a alteracao, vai multiplicar a linha do pivo (ver caderno)

    // zerando parte inferior
    for(posPivo = 0; posPivo < linhas; posPivo++)
    {
        for(int l = lInicio; l < linhas; l++)
        {
            multiplicador = escalonada.matriz[l][posPivo] / escalonada.matriz[posPivo][posPivo];
            for(int c = posPivo; c < colunas; c++)
            {
                escalonada.matriz[l][c] = escalonada.matriz[l][c] - multiplicador * escalonada.matriz[posPivo][c];
            }
        }
        //escalonada.imprimir();
        ++lInicio;
    }
    return escalonada;
}

// triangular superior = zera o triangulo de baixo
vector<Matriz> Matriz::gaussPivoteamento(Matriz& vetorSolucao) const
{
    Matriz escalonada(linhas, colunas);

    escalonada.matriz = matriz;

    Matriz Operacoes(linhas, 2); // para guardar em que posicao esta a linha

    for(int l = 0; l < linhas; l++)
    {
        Operacoes[l][0] = l;
        Operacoes[l][1] = l;
    }

    vector<Matriz> MatrizEOperacoes;

    int posPivo; // posicao do pivo/coluna dos "pivos" de cada linha abaixo

    int lInicio = 1; // eh 1 pq a primeira linha (0) nao sofre alteracao

    float multiplicador; // eh o primeiroDaLinha/pivo
    // pivo ta sempre na diagonal, eh o que vai multiplicar a linha que esta sofrendo a alteracao (ver caderno)
    // primeiroDaLinha eh o primeiro numero que nao eh zero na linha que esta sofrendo a alteracao, vai multiplicar a linha do pivo (ver caderno)

    int maior;

    // zerando parte inferior
    for(posPivo = 0; posPivo < linhas; posPivo++)
    {
        // pivoteamento
        maior = posPivo;
        for(int l = 0; l < linhas; l++)
        {
            if(escalonada.matriz[l][posPivo] > escalonada.matriz[maior][posPivo])
            {
                maior = l;
            }
        }
        if(maior != posPivo)
        {
            for(int c = 0; c < colunas; c++)
            {
                escalonada.matriz[posPivo][c] = escalonada.matriz[maior][c];
                escalonada.matriz[maior][c] = matriz[posPivo][c];
            }
            matriz = escalonada.matriz;
            Operacoes[posPivo][0] = maior;
            Operacoes[maior][0] = posPivo;
        }

        // gauss
        for(int l = lInicio; l < linhas; l++)
        {
            multiplicador = escalonada.matriz[l][posPivo] / escalonada.matriz[posPivo][posPivo];
            for(int c = posPivo; c < colunas; c++)
            {
                escalonada.matriz[l][c] = escalonada.matriz[l][c] - multiplicador * escalonada.matriz[posPivo][c];
            }
            // alterando os valores do vetorSolucao tambem
            for(int i = 0; i < vetorSolucao.colunas; i++)
            {
                vetorSolucao.matriz[l][i] = vetorSolucao.matriz[l][i] - multiplicador * vetorSolucao.matriz[posPivo][i]; // faz as operacoes no vetorSolucao tambem, alem de na matriz
            }
        }
        //escalonada.imprimir();
        ++lInicio;
    }

    MatrizEOperacoes.push_back(escalonada);
    MatrizEOperacoes.push_back(Operacoes);

    return MatrizEOperacoes;
}


Matriz Matriz::gaussJordan(Matriz& vetorSolucao) const
{
    Matriz escalonada(linhas, colunas);

    escalonada = gauss(vetorSolucao);

    int posPivo;

    for(posPivo = linhas - 1; posPivo >= 0; posPivo--)
    {
        float divisor = escalonada.matriz[posPivo][posPivo];
        // fazendo a diagonal 1
        for(int c = posPivo; c < linhas; c++)
        {
            escalonada.matriz[posPivo][c] = escalonada.matriz[posPivo][c] / divisor;
        }
        // fazendo no vetor solucao tambem
        for(int i = 0; i < vetorSolucao.colunas; i++)
        {
            vetorSolucao.matriz[posPivo][i] = vetorSolucao.matriz[posPivo][i] / divisor; // faz as operacoes no vetorSolucao tambem, alem de na matriz
        }

        // zerando a parte de cima
        for(int l = 0; l < posPivo; l++)
        {
            float multiplicador = escalonada.matriz[l][posPivo] / escalonada.matriz[posPivo][posPivo];
            for(int c = 0; c < colunas; c++)
            {
                escalonada.matriz[l][c] = escalonada.matriz[l][c] - multiplicador * escalonada.matriz[posPivo][c];
            }
            // fazendo no vetor solucao tambem
            for(int i = 0; i < vetorSolucao.colunas; i++)
            {
                vetorSolucao.matriz[l][i] = vetorSolucao.matriz[l][i] - multiplicador * vetorSolucao.matriz[posPivo][i]; // faz as operacoes no vetorSolucao tambem, alem de na matriz
            }
        }
        //escalonada.imprimir();
        //vetorSolucao.imprimir();
    }
    return escalonada;
}


Matriz Matriz::gaussJordan() const
{
    Matriz escalonada(linhas, colunas);

    escalonada = gauss();

    int posPivo;

    for(posPivo = linhas - 1; posPivo >= 0; posPivo--)
    {
        float divisor = escalonada.matriz[posPivo][posPivo];
        // fazendo a diagonal 1
        for(int c = posPivo; c < linhas; c++)
        {
            escalonada.matriz[posPivo][c] = escalonada.matriz[posPivo][c] / divisor;
        }

        // zerando a parte de cima
        for(int l = 0; l < posPivo; l++)
        {
            float multiplicador = escalonada.matriz[l][posPivo] / escalonada.matriz[posPivo][posPivo];
            for(int c = 0; c < colunas; c++)
            {
                escalonada.matriz[l][c] = escalonada.matriz[l][c] - multiplicador * escalonada.matriz[posPivo][c];
            }
        }
        //escalonada.imprimir();
    }
    return escalonada;
}

Matriz Matriz::gaussJordanPivoteamento(Matriz& vetorSolucao) const
{
    Matriz escalonada(linhas, colunas);

    Matriz Operacoes(linhas, 2); // para guardar em que posicao esta a linha

    vector<Matriz> MatrizEOperacoes;

    escalonada = gauss(vetorSolucao);

    int posPivo;

    for(posPivo = linhas - 1; posPivo >= 0; posPivo--)
    {
        float divisor = escalonada.matriz[posPivo][posPivo];
        // fazendo a diagonal 1
        for(int c = posPivo; c < linhas; c++)
        {
            escalonada.matriz[posPivo][c] = escalonada.matriz[posPivo][c] / divisor;
        }
        // fazendo no vetor solucao tambem
        for(int i = 0; i < vetorSolucao.colunas; i++)
        {
            vetorSolucao.matriz[posPivo][i] = vetorSolucao.matriz[posPivo][i] / divisor; // faz as operacoes no vetorSolucao tambem, alem de na matriz
        }

        // zerando a parte de cima
        for(int l = 0; l < posPivo; l++)
        {
            float multiplicador = escalonada.matriz[l][posPivo] / escalonada.matriz[posPivo][posPivo];
            for(int c = 0; c < colunas; c++)
            {
                escalonada.matriz[l][c] = escalonada.matriz[l][c] - multiplicador * escalonada.matriz[posPivo][c];
            }
            // fazendo no vetor solucao tambem
            for(int i = 0; i < vetorSolucao.colunas; i++)
            {
                vetorSolucao.matriz[l][i] = vetorSolucao.matriz[l][i] - multiplicador * vetorSolucao.matriz[posPivo][i]; // faz as operacoes no vetorSolucao tambem, alem de na matriz
            }
        }
        //escalonada.imprimir();
        //vetorSolucao.imprimir();
    }

    MatrizEOperacoes.push_back(escalonada);
    MatrizEOperacoes.push_back(Operacoes);

    return MatrizEOperacoes;
}


// da as solucoes para os x's depois de ter feito gauss
Matriz Matriz::retroSubstituicao(Matriz& vetorSolucao) const
{
    Matriz solucao(vetorSolucao.linhas, vetorSolucao.colunas); // nova matriz que vai guardar os resultados dos x's

    for(int i = 0; i < vetorSolucao.colunas; i++)
    {
        for (int l = linhas - 1; l >= 0; l--) 
        {
            float somatorio = 0;
            for (int c = colunas - 1; c >= 0; c--) 
            {
                somatorio += matriz[l][c] * solucao.matriz[c][i];
            }
            solucao.matriz[l][i] = (vetorSolucao.matriz[l][i] - somatorio) / matriz[l][l]; 
        }
    }

    return solucao;
}

vector<Matriz> Matriz::decomposicaoLU() const 
{ 
    Matriz L(linhas, colunas); 
    Matriz U(linhas, colunas); 
    vector<Matriz> A;
    vector<float> multiplicadores;

    U.matriz = matriz;
    L.matriz = matriz;

    L = L.gaussJordan();

    int posPivo; // posicao do pivo/coluna dos "pivos" de cada linha abaixo

    int lInicio = 1; // eh 1 pq a primeira linha (0) nao sofre alteracao

    float multiplicador; // eh o primeiroDaLinha/pivo
    // pivo ta sempre na diagonal, eh o que vai multiplicar a linha que esta sofrendo a alteracao (ver caderno)
    // primeiroDaLinha eh o primeiro numero que nao eh zero na linha que esta sofrendo a alteracao, vai multiplicar a linha do pivo (ver caderno)

    // zerando parte inferior
    for(posPivo = 0; posPivo < linhas; posPivo++)
    {
        for(int l = lInicio; l < linhas; l++)
        {
            multiplicador = U.matriz[l][posPivo] / U.matriz[posPivo][posPivo];
            multiplicadores.push_back(multiplicador);
            for(int c = posPivo; c < colunas; c++)
            {
                U.matriz[l][c] = U.matriz[l][c] - multiplicador * U.matriz[posPivo][c];
            }
            // alterando os valores do vetorSolucao tambem
            /*for(int i = 0; i < vetorSolucao.colunas; i++)
            {
                vetorSolucao.matriz[l][i] = vetorSolucao.matriz[l][i] - multiplicador * vetorSolucao.matriz[posPivo][i]; // faz as operacoes no vetorSolucao tambem, alem de na matriz
            }*/
        }
        ++lInicio;
    }

    for(int c = colunas - 1; c >= 0; c--)
    {
        for(int l = linhas - 1; l > c; l--)
        {
            multiplicador = multiplicadores.back();
            multiplicadores.pop_back();
            L.matriz[l][c] = multiplicador;
        }
    }

    A.push_back(L);
    A.push_back(U);

    return A; 
}

vector<Matriz> Matriz::cholesky() const
{
    Matriz G(linhas, colunas);
    vector<Matriz> MatrizEOperacoes;
    Matriz Operacoes(linhas, 2); // para guardar em que posicao esta a linha

    float somatorio;

    for(int l = 0; l < linhas; l++)
    {
        // para a diagonal  
        somatorio = 0;
        for(int k = 0; k < l; k++)
        {
            somatorio += G.matriz[l][k] * G.matriz[l][k];
        }
        G.matriz[l][l] = sqrt(matriz[l][l] - somatorio); 

        // demais elementos
        somatorio = 0;
        for(int c = l + 1; c < colunas; c++)
        {
            for(int k = 0; k < l; k++)
            {
                somatorio += G.matriz[l][k] * G.matriz[c][k];
            }
            G.matriz[c][l] = (matriz[l][c] - somatorio)/G.matriz[l][l];
        }   
    }


    MatrizEOperacoes.push_back(G);

    G.imprimir();

    return MatrizEOperacoes;
}


#include <iostream>
#include "matriz.h"
using namespace std;

int main(int argc, char *argv[]) {

    Matriz matrizB(2, 2, { 
        {1, 3}, 
        {2, 4}
    });

    Matriz matrizC(2, 2, { 
        {5, 7}, 
        {6, 8}
    });

    //Matriz matrizA(4, 4); // tem que usar o setElemento depois

    // linha, coluna, numero, outro jeito de setar a matriz
    /*matrizA.setElemento(0, 0, 2);
    matrizA.setElemento(0, 1, 3);
    matrizA.setElemento(0, 2, 1);
    matrizA.setElemento(0, 3, 0);

    matrizA.setElemento(1, 0, 3);
    matrizA.setElemento(1, 1, 1);
    matrizA.setElemento(1, 2, 2);
    matrizA.setElemento(1, 3, 1);

    matrizA.setElemento(2, 0, 2);
    matrizA.setElemento(2, 1, 1);
    matrizA.setElemento(2, 2, 3);
    matrizA.setElemento(2, 3, 4);

    matrizA.setElemento(3, 0, 1);
    matrizA.setElemento(3, 1, 2);
    matrizA.setElemento(3, 2, 4);
    matrizA.setElemento(3, 3, 3);*/

    cout << "----------------" << endl;
    cout << "SOMA DE MATRIZES" << endl;
    cout << "----------------" << endl;

    Matriz resultadoSoma = matrizB + matrizC;

    cout << "\nMatriz B: " << endl;
    matrizB.imprimir();

    cout << "\nMatriz C: " << endl;
    matrizC.imprimir();

    cout << "\nMatriz B + C: " << endl;
    resultadoSoma.imprimir();
    printf("\n");

    cout << "---------------------" << endl;
    cout << "SUBTRAÇÃO DE MATRIZES" << endl;
    cout << "---------------------" << endl;

    Matriz resultadoSubtracao = matrizB - matrizC;

    cout << "\nMatriz B - C: " << endl;
    resultadoSubtracao.imprimir();
    printf("\n");

    cout << "-------------------------" << endl;
    cout << "MULTIPLICAÇÃO DE MATRIZES" << endl;
    cout << "-------------------------" << endl;

    Matriz resultadoMultiplicacao = matrizB * matrizC;

    cout << "\nMatriz B * C: " << endl;
    resultadoMultiplicacao.imprimir();
    printf("\n");

    cout << "----------------" << endl;
    cout << "SOMA POR ESCALAR" << endl;
    cout << "----------------" << endl;

    Matriz resultadoSomEsc = matrizB + 2;

    cout << "\nMatriz B + 2: " << endl;
    resultadoSomEsc.imprimir();
    printf("\n");

    cout << "---------------------" << endl;
    cout << "SUBTRAÇÃO POR ESCALAR" << endl;
    cout << "---------------------" << endl;

    Matriz resultadoSubEsc = matrizB - 2;

    cout << "\nMatriz B - 2: " << endl;
    resultadoSubEsc.imprimir();
    printf("\n");

    cout << "-------------------------" << endl;
    cout << "MULTIPLICAÇÃO POR ESCALAR" << endl;
    cout << "-------------------------" << endl;

    Matriz resultadoMultEsc = matrizB * 2;

    cout << "\nMatriz B * 2: " << endl;
    resultadoMultEsc.imprimir();
    printf("\n");

    cout << "-------------------" << endl;
    cout << "DIVISÃO POR ESCALAR" << endl;
    cout << "-------------------" << endl;

    Matriz resultadoDivEsc = matrizB / 2;

    cout << "\nMatriz B / 2: " << endl;
    resultadoDivEsc.imprimir();
    printf("\n");

    cout << "-----" << endl;
    cout << "GAUSS" << endl;
    cout << "-----" << endl;

    // A.x = B

    Matriz matrizAGauss(4, 4, { // A
        {1, 1, 1, 2}, // coluna 0
        {3, 2, 1, 1}, // coluna 1
        {1, 2, 1, 2}, // coluna 2
        {1, 1, 1, 1} // coluna 3
    });

    Matriz vetorBGauss (4, 1, {  // B
        {7, 4, 3, 3}
    });

    Matriz resultadoGauss = matrizAGauss.gaussPivo(vetorBGauss); // A escalonada

    Matriz solucaoGauss = resultadoGauss.retroSubstituicao(vetorBGauss); // x


    printf("\n");
    cout << "matriz A inicial:" << endl;
    matrizAGauss.imprimir();  // A
    printf("\n");

    cout << "resultado/matriz escalonada:" << endl;
    resultadoGauss.imprimir(); // A escalonada
    printf("\n");

    cout << "vetores solucao:" << endl;
    vetorBGauss.imprimir(); // B
    printf("\n");

    cout << "substituicao/solucao dos x's:" << endl;;
    vetorXGauss.imprimir(); // x
    printf("\n");

    cout << "------------" << endl;
    cout << "GAUSS-JORDAN" << endl;
    cout << "------------" << endl;

    // A.x = B

    Matriz matrizAGaussJordan(3, 3, { // A
        {3, 1, 4}, // coluna 0
        {2, 1, 3}, // coluna 1
        {4, 2, -2}, // coluna 2
    });

    Matriz vetorBGaussJordan (3, 1, { // B
        {1, 2, 3}
    });

    Matriz resultadoGaussJordan = matrizTeste.gaussJordan(vetorBGaussJordan); // matriz escalonada reduzida

    Matriz vetorXGaussJordan = resultadoTeste.retroSubstituicao(vetorBGaussJordan); // x

    printf("\n");
    cout << "matriz A inicial:" << endl;
    matrizAGaussJordan.imprimir();
    printf("\n");

    cout << "resultado/matriz escalonada:" << endl;
    resultadoGaussJordan.imprimir();
    printf("\n");

    cout << "vetores solucao:" << endl;
    vetorBGaussJordan.imprimir();
    printf("\n");

    cout << "substituicao/solucao dos x's:" << endl;;
    vetorXGaussJordan.imprimir();
    printf("\n");

    cout << "---------------" << endl;
    cout << "DECOMPOSIÇÃO LU" << endl;
    cout << "---------------" << endl;

    vector<Matriz> A = matrizD.decomposicaoLU();

    printf("L=\n");
    A[0].imprimir();
    printf("\n");
    printf("U=\n");
    A[1].imprimir();
    printf("\n");

    Matriz mA = A[0] * A[1];
    mA.imprimir();
    printf("\n");

    cout << "--------" << endl;
    cout << "CHOLESKY" << endl;
    cout << "--------" << endl;

    Matriz matrizTesteCho(3, 3, {
        {4, -2, 2}, // coluna 0
        {-2, 10, -7}, // coluna 1
        {2, -7, 6}, // coluna 2
    });

    matrizTesteCho.cholesky();

    return 0;
}
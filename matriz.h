#ifndef __MATRIZ_H__
#define __MATRIZ_H__

#include <iostream>
#include <vector>
#include <cmath>
using namespace std;

class Matriz {
    public:
        int linhas; // quantidade de linhas
        int colunas; // quantidade de colunas
        vector<vector<float>> matriz; // vector de vectores que guarda os numeros em si

        Matriz(int linhas, int colunas); // inicializa so com as quantidades, pra depois colocar os numeros com o setElemento
        Matriz(int linhas, int colunas, vector<vector<float>> solucao); // inicializa com as quantidades e ja colocando um vector de vectors tambem (caso de vetores solucao, por ex.)
        ~Matriz(); // destrutor

        void setElemento(int linha, int coluna, int valor); // seta os numeros
        int getElemento(int linha, int coluna) const; // retorna um numero 
        void imprimir() const; // imprime a matriz

        // operacoes entre matrizes
        Matriz operator+(const Matriz& n) const; // soma matrizes
        Matriz operator-(const Matriz& n) const; // subtrai matrizes
        Matriz operator*(const Matriz& n) const; // multiplica matrizes

        // operacoes entre matriz e escalar
        Matriz operator+(int i) const; // soma matriz com escalar
        Matriz operator-(int i) const; // subtrai escalar de matriz
        Matriz operator*(int i) const; // multiplica matriz por escalar
        Matriz operator/(int i) const; // divide matriz por escalar

        // outras funcoes (escalonamento, etc.)
        Matriz T() const;
        Matriz gauss(Matriz& vetorSolucao) const; // zera triangulo inferior
        Matriz gauss() const;
        vector<Matriz> gaussPivoteamento(Matriz& vetorSolucao) const;
        Matriz gaussJordan(Matriz& vetorSolucao) const; // matriz identidade
        Matriz gaussJordan() const;
        vector<Matriz> gaussJordanPivoteamento(Matriz& vetorSolucao) const;
        Matriz retroSubstituicao(Matriz& vetorSolucao) const; // da as solucoes para os x's depois de ter feito gauss
        vector<Matriz> decomposicaoLU() const;
        vector<Matriz> cholesky() const;
};


#endif